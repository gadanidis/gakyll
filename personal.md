---
title: Personal
---

![](images/me.jpg "An image of me.")


I grew up in London, Ontario and went to high school in Whitby, Ontario. I've
been living in Toronto since 2013, where I live with my partner,
[Aileen](https://www.aileenlin.com), and our two rabbits: Lulu (white) and Bean
(brown).

![](images/rabbits.png "An image of our two rabbits, Lulu and Bean, standing in/over a bowl of food.")

Other than linguistics, my interests include reading, playing board and video
games, programming, and listening to and playing music.

Other than linguistics, some of my interests include reading, playing board and
video games, free and open software (GNU/Linux, etc.), and music.

## Programming

One of my favourite ways to procrastinate is writing code.
Most of my day-to-day data analysis is done using
[Python](https://www.python.org/), and I also use Python to write small scripts
to make my life easier.

I also really enjoy using [Haskell](https://www.haskell.org/).
(This site is built using [Hakyll](http://jaspervdj.be/hakyll), a Haskell
library for generating static sites.)

My current side project is writing an economic simulation game in
[Rust](https://www.rust-lang.org/) with [Bevy](https://bevyengine.org/).
This is still in the very early stages so I haven't pushed the code yet but I'll
link it here when I do.

You can find my code at [GitLab](https://gitlab.com/gadanidis).
I also have a [Github account](https://github.com/gadanidis) which I use to
follow and contribute to open-source projects that are hosted there.

It's not really "programming" per se, but I set up the website for [Renshin
Kendo Club](https://renshinkendo.ca).

## Music

I've played the violin since I was little and I love listening to and
discovering new music.
Over the last couple years I've been spending most of my violin time playing
Irish traditional music.
You can check out [my profile on The
Session](https://thesession.org/members/142304) if you're curious about what
I've been playing.
