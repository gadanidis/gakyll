---
title: Caught by MuseScore's dark patterns
---

I'm a little embarrassed to write this post as I'm normally very careful about
stuff like this, but this week I was successfully tricked by a [dark
pattern](https://www.darkpatterns.org/).
From [darkpatterns.org](https://www.darkpatterns.org/):

> **Dark Patterns** are tricks used in websites and apps that make you do things
> that you didn't mean to, like buying or signing up for something.

## MuseScore

The site that got me is [musescore.com](https://musescore.com), which is the
online music score sharing platform that complements the free
[MuseScore](https://en.wikipedia.org/wiki/MuseScore) scorewriter program.
The MuseScore website provides downloadable scores for music in the public
domain, MuseScore users' original compositions, and arrangements of copyrighted
works.
Public-domain music is available to download with a free account, but to
download original compositions or arrangements of copyrighted works, you need a
paid subscription.

The ethics of this business model, and copyright in general, are a whole other
can of worms that I won't get into here, but I do want to note that even if they
hadn't engaged in deceptive design practices, you should definitely boycott
them because their Director of Strategy [publicly threatened to get someone
deported for alleged copyright
violations](https://twitter.com/marcan42/status/1417085393762099200) --- I only
learned about this when doing research for this blog post but it's absolutely
vile.

## Taking the bait

Anyway, I wanted to download an arrangement of the jazz standard [Autumn
Leaves](https://en.wikipedia.org/wiki/Autumn_Leaves_(1945_song)) by [Joseph
Kozma](https://en.wikipedia.org/wiki/Joseph_Kosma) for piano and violin to play
along with my partner.
To download user-created arrangements, you must pay for a "PRO" subscription,
but MuseScore offers a [1-week free trial which automatically converts into a
yearly paid subscription at the end of the trial
period](https://www.darkpatterns.org/types-of-dark-pattern/forced-continuity).
No problem --- I just wanted to download this one score, so I signed up for the
free trial intending to download it and immediately cancel.

It turns out MuseScore uses a tricky ['roach
motel'](https://www.darkpatterns.org/types-of-dark-pattern/roach-motel) dark
pattern --- it's easy to get into the free trial, but it's hard to get out.
In fact, when you create your account and start the free trial, you are *unable
to manage your subscription settings* for at least one hour.
[MuseScore claims this is because "your account is being set
up"](https://help.musescore.com/hc/en-us/articles/209540009) but this is
obviously bullshit as you can immediately begin using all of your account
features --- downloading scores, editing your profile, etc. --- except for
editing your subscription.
The real reason you are locked out of editing your subscription is obvious:
MuseScore hopes you'll start the trial, download one or two scores, then forget
about it until you get hit with the automatic yearly charge at the end of the
trial period.
Unfortunately, that's exactly just what happened to me because I forgot to set a
reminder to cancel the trial, foolishly trusting my faulty human memory; I was
charged for the yearly subscription on Monday morning.

## Getting off the hook

This sucks, but there's little to be done about it --- I should've set a
reminder!
I immediately logged in to cancel the auto-renewal so that I don't get hit with
the renewal fee in a year, and MuseScore is using even more dark patterns in the
subscription cancellation dialog box, which I've included a screenshot of below.

![](/images/musescore_cancellation_dialog.png "The dialog box to end the
MuseScore subscription. There are two buttons, one reading 'I want to keep my
benefits' and the other reading 'I do not want my benefits'.")

At the top it says "your benefits will remain active for the current paid
period" --- they'd better, given how much I was just charged for the yearly
subscription!
However, to end the subscription you have to click the button that says "I do
not want my benefits", which makes it seem like you'll immediately lose access
if you click on it (you don't).
This is known as
['confirmshaming'](https://www.darkpatterns.org/types-of-dark-pattern/confirmshaming)
and is intended to discourage you from cancelling.

The buttons are also styled in a misleading way:
The button you need to click, "I do not want my benefits", is styled like a
cancel button which you'd expect to close the window without taking any action
(coloured the same as the background, on the right side of the screen).
In contrast, the "I want to keep my benefits" button, which is styled like a
confirmation button (presented in a more prominent colour, on the left side of
the screen) is actually the 'cancel' button, which does not end the
subscription.
If you don't read the text carefully, it would be easy to think that your
subscription has already been cancelled and this dialog box is to decide whether
you want to keep the benefits for the remainder of the subscription period.

## Getting some money back

The [MuseScore terms of service](https://musescore.com/legal/terms) state:

>  if you cancel your subscription during the first fourteen (14) days of your
>  subscription term you will receive a refund of the difference between the
>  then-in-effect and current subscription fee to which You are subscribed and
>  the then-in-effect and current monthly subscription fee. Your account will
>  not auto-renew for any recurring periodic charges at the end of the
>  subscription term. As a courtesy to you we will convert your Account to a
>  free account at the end of the 30-day period.

You can't actually cancel your subscription online; you can only cancel the
auto-renewal.
To cancel your actual subscription and get a partial refund, you have to submit
a support request (another [roach
motel](https://www.darkpatterns.org/types-of-dark-pattern/roach-motel)).

According to [this Reddit
post](https://www.reddit.com/r/Musescore/comments/pp39b6/regarding_refund/),
though, it seems MuseScore neglects to actually inform you about their own terms
of service when you request a refund, and they instead offer you a 35% refund,
far below what you are actually entitled to.
I didn't want to deal with any of that so I included a quotation from the ToS in
my refund request, and said:

> In line with the ToS, please cancel my subscription and refund the difference
> between the yearly subscription fee and the current monthly subscription fee.

Not to be deterred, though, the support desk *still* told me that they talked to
the head office and were able to negotiate a special offer for me: either a free
extension of my subscription or a 35% refund.
They even did the button thing again!
The option they want you to click (no refund) is in green and on the left, while
the refund option (which is actually less than you're entitled to) is in grey
and on the right.

![](/images/musescore_email.png "Screenshot of an email from MuseScore support
offering me either 10 extra months or a 35% refund by clicking one of two
buttons, with text beneath noting that the maximum refund is actually 60 USD.")

To access the maximum refund you're entitled to, which gets just a passing
mention beneath the two more prominent options, there's no button.
You have to email back again, which I did, and the refund has finally just gone
through.

## Coda

I'm disappointed in myself for getting tricked, but I'm even more disappointed
to see an online service associated with a [free and open source
scorewriter](https://github.com/musescore/MuseScore) engaging in such
misleading, dishonest, and anti-human design practices.

Unfortunately, deceptive business practices seem to be par for the course for
[Muse Group](https://mu.se), MuseScore's parent company.
They were recently in the news after acquiring
the popular audio-editing software [Audacity](https://www.audacityteam.org/) and
[quietly adding spyware](https://news.ycombinator.com/item?id=27727150).
They apologized and pinky-promised they wouldn't do it again, but you should
probably switch to [Tenacity](https://github.com/tenacityteam/tenacity) anyway.
And if you're looking for open-source music transcription software to replace
MuseScore, you can't do better than [GNU LilyPond](https://lilypond.org/).
