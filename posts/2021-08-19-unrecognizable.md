---
title: "'Unrecognizable' and reliance on knowledge of English in linguistics evaluations"
---

## The problem

I was recently talking with some colleagues about the following question about
structural ambiguity from an assignment in an introductory syntax course:

> There is more than one way to form the word “unrecognizable” with the three
> morphemes un-, recognize, -able. True or false?

The intended answer was *false*: the only way to form "unrecognizable" is by
first combining "recognize" and "-able" to make "recognizable", then attaching
"un-".
However, a student who had answered *true* wrote to the instructor to complain,
insisting that "unrecognizable" was structurally ambiguous.
This argument was based on the purported acceptability of the word "unrecognize"
in the context of someone suffering from memory problems and no longer being
able to recognize someone (e.g., "After developing Alzheimer's I unrecognized my
own child.")

As an L1 English speaker, to me "unrecognize" is clearly not a valid
construction except perhaps in a political context (e.g., "The Trump
administration unrecognized the Maduro administration"), and I would be
hard-pressed to find a context where I would ever then say "The Maduro
administration was unrecognizable".
If anything, it sounds like language play.
But the student's argument for their answer clearly indicated that they
understood the concept of structural ambiguity: if "unrecognize" is a valid
construction, we should be able to get "unrecognizable" from it by adding
"-able".
If we accept that they thought "unrecognize" was possible, their only error was
in their knowledge of English vocabulary.
The point of the question was structural ambiguity, not English vocabulary.
So should they lose the mark?
I've run into this dilemma many times as a teaching assistant in syntax courses.
It can be very tricky to write and grade syntax problems which depend on English
lexical or syntactic knowledge, especially when many of our students are not L1
English speakers.

My position on these issues as a teaching assistant and grader has generally
been that we should be generous with our students and grade them as if their
judgments about English are correct.
But that can be problematic in cases like the example above, which is supposed
to be a 1-point true-or-false question, with no space for students to justify
their answer.
Unless they tell us after the fact, we can't know if their answer was wrong
because they didn't understand structural ambiguity or because of their English
vocabulary --- giving everyone the benefit of the doubt would mean giving
everyone full marks, no matter how they answered.
The question therefore does not cleanly distinguish between students who
understand structural ambiguity and those who do not.

## Solutions

One solution is to **modify the question**.
If a question depends on some knowledge of English vocabulary, can it be
rewritten so that it doesn't?
This may require changing the type of the question from a simple true/false to a
more complex short-answer one:

> Explain how we can determine if a word is structurally ambiguous.

Or we could ask it as multiple-choice:

> A word is structurally ambiguous if:
>
> - a) The word can be formed by combining its constituent morphemes in multiple
> different ways.
> - b) The word has multiple different meanings.
> - c) ...

Or can we **provide students with the necessary vocabulary knowledge** in the
question itself?

> There is more than one way to form the word “unrecognizable” with the three
> morphemes un-, recognize, -able. True or false? *Assume that "unrecognize" is
> not a valid construction in English, but "recognizable" is.*

If providing this information makes the question feel too easy, that should
inspire some reflection about what your question is really testing!

Another is to **make the instructor and/or TA(s) available to provide
judgments** for such problems, and state this explicitly in the question (or in
the assessment instructions) so students know what to do.

> There is more than one way to form the word “unrecognizable” with the three
> morphemes un-, recognize, -able. True or false? *You can ask your TA for
> a judgment if you aren't sure if a construction is valid in English.*

All of these options even out the playing field for L1 and L2 speakers by
removing or deemphasizing the required English knowledge.
These all make the question better, but I think a better way to solve this
problem is to just avoid asking questions about the language of instruction at
all.
This stops us from relying on the (often false) assumption that our students are
comfortable enough in English to make acceptability judgments about it and
forces us to write questions that deal exclusively with *linguistics* knowledge
as opposed to *language* knowledge.

<!-- vim: tw=80 fo+=t ft=markdown
-->
