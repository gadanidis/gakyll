---
title: Research
---

I am a sociolinguist: my research centers on how society and language are
intertwined and how they affect one another.

I use a mixture of statistical/computational and qualitative methods to
investigate sociolinguistic questions.
Some view quantitative methods and critical qualitative methods as incompatible,
but I think they complement each other very well!
Quantitative methods are great for identifying broad patterns and not so great
for understanding language in context; qualitative methods are great for
understanding micro-level aspects of language in context but can be difficult to
apply to large datasets.
In my research I try to bridge the gap between these seemingly disparate
approaches.

I'm always happy to answer questions about my research at
[timothy.gadanidis@mail.utoronto.ca](mailto:timothy.gadanidis@mail.utoronto.ca).
I'd also be thrilled to talk about potential collaboration, especially if your
interests overlap with mine, or if you're a qualitative analyst who is
quant-curious (or vice versa!).

## Primary research interests

### Dissertation: Personal narratives of restaurant service

I am currently working on my dissertation, which is a discourse analysis of
narratives told by restaurant servers about restaurant service during COVID-19.
Some of the questions I'm interested in are:

- In what ways is the language of restaurant service workers commodified ---
valued, devalued, exchanged directly or indirectly for income, etc., and how
does this influence their discourse strategies?
- Who has power in the recounted service encounters, and how does this influence
discourse?
- How do restaurant service workers construct their identities through language
as they convert their experiences at work into narratives?
- How do servers navigate the tension between the persona they take on at work
and their identity outside the workplace?
- What are servers' goals in telling stories about their experiences at work?
- How is all of this affected by the climate around COVID-19 and the subsequent
restaurant labour shortage?

I'm currently working on an analysis of narratives from the Reddit storytelling
forum [/r/TalesFromYourServer](https://reddit.com/r/TalesFromYourServer).
Later, I plan to conduct some oral interviews to elicit narratives from people
face-to-face (or monitor-to-monitor as the case may be).

For more information and some related references, you can [download the slides
from my thesis proposal defense](/pdf/proposal_slides_tg.pdf).

### Online climate discourse

My second qualifying paper ([download](/pdf/gp2.pdf "download my second
qualifying paper")) was a computational analysis of of data from five different
Reddit communities that focus on climate change:
[/r/climate](https://reddit.com/r/climate),
[/r/climate\_science](https://reddit.com/r/climate_science),
[/r/ClimateOffensive](https://reddit.com/r/ClimateOffensive),
[/r/collapse](https://reddit.com/r/collapse), and
[/r/climateskeptics](https://reddit.com/r/climateskeptics).
Perhaps unsurprisingly, the way these communities talk about climate change
reflects their communal ideologies about it, and this can be detected
computationally and analyzed qualitatively.
For example, in a [classification
model](https://en.wikipedia.org/wiki/Statistical_classification), the words
*agenda* and *narrative*, which characterize their referent as fictional or
constructive, are distinctive of /r/climateskeptics (whose users tend to believe
that climate change is either completely made up or not as bad as 'alarmists',
i.e., mainstream scientists, journalists and activists, claim).

There's a lot to talk about, but I think one of the main contributions of the
paper is to show that, contrary to their popular image as anti-science deniers,
climate-change "skeptics" do not dispute the value of science.
What they actually claim is that *prominent climate-science deniers are the real
scientists* and that actual climate scientists have abandoned the scientific
method.
They perform this ideology through their discourse: they use scientific jargon
almost as much as the users of /r/climate\_science!
This shows that online skeptics are mirroring discourse strategies used by large
fossil-fuel-industry-funded think tanks like the Heartland Institute (see [this
fascinating paper by Taylor-Neu
2020](https://anthrosource.onlinelibrary.wiley.com/doi/full/10.1111/jola.12257)
for discussion and analysis of this).

This work was supervised by [Barend
Beekhuizen](http://www.cs.toronto.edu/~barend/), the second reader was [Atiqa
Hachimi](https://www.utsc.utoronto.ca/hcs/atiqa-hachimi), and the third reader
was [Nathan Sanders](http://sanders.phonologist.org/).

### *Um* and *uh*

For my MA degree paper ([download](/pdf/ma_forum_paper.pdf "download my MA
degree paper")), I conducted a study of *um* and *uh* in instant messaging (IM). Recent
variationist work (e.g., Wieling et al., 2016) has identified a change in
progress: *um* is rising in frequency relative to *uh*, which might be related
to an emerging functional difference between the two. My goal was to explore
that idea using IM data, in which the use of the words seems to be more
intentional: while some would argue we “automatically” produce them in speech,
using them in IM requires more conscious effort and thus may provide clues as to
their discourse-pragmatic function.
This work was supervised by [Derek Denis](https://derekdenis.com/), and the
second reader was [Sali
Tagliamonte](http://individual.utoronto.ca/tagliamonte/).

For my first PhD qualifying paper ([download](/pdf/gp1.pdf "download my
first qualifying paper")),
I extended the findings of my MA research with an
[experiment](https://gitlab.com/uhum-perceptions) testing readers' and
listeners' social and interactional perceptions of speakers depending on whether
they use *um*, *uh*, or *neither*.
I found that while both *um* and *uh* are perceived as hesitant, *um* was
perceived as more feminine, more polite and more thoughtful than both *uh* and
*neither*.
I argue that, at least for my participants, *uh* indexes just plain hesitation,
and *um* indexes a *specific type* of thoughtful and feminine hesitation.
This work was supervised by [Derek Denis](https://derekdenis.com), [Jessamyn
Schertz](https://www.individual.utoronto.ca/jschertz) was the second reader, and
[Atiqa Hachimi](https://www.utsc.utoronto.ca/hcs/atiqa-hachimi) was the third
reader.

Derek and I have also analyzed *um* and *uh* in corpus data from Ontario farmers
born in the late 1800s and early 1900s: a time before the rise of *um*.
We find that during this time period, it's *uh* that increases in frequency, not
*um*; and it seems to have expanded its functional range, appearing more
frequent in non-sentence-initial contexts.
We speculate this may be due to a general increase in mid-sentence
pause-filling, and that *uh* was recruited by speakers to fill pauses that may
have previously gone unfilled.

## Other projects

### Speech rate normalization in Japanese

This is joint work with [Yoonjung Kang](https://www.yoonjungkang.com/).
Using a series of online experiments, we've been investigating how Japanese
speakers' perceptions of short and long consonants and vowels differ depending
on how fast the surrounding speech rate is (*perceptual speech rate
normalization*), and investigating the many factors that affect it.

### Covariation and complexity in heritage languages

This is joint work with [Naomi Nagy](https://individual.utoronto.ca/ngn/) that
builds on the efforts of dozens of current and former research assistants on the
[Heritage Language Variation and Change (HLVC)
project](https://ngn.artsci.utoronto.ca/HLVC/0_0_home.php).

Using the HLVC corpora, we've been investigating:

- *covariation*: do the speakers who use one 'innovative' linguistic feature
also tend to use others? (it's complicated, but doesn't really seem like it!)
- *complexity*: are heritage languages really 'simpler' versions of homeland
varieties, as is sometimes claimed? (doesn't seem like it!)

### Projects I haven't found time to write a little blurb for yet

- [Integrating qualitative and quantitative analyses of stance: A case study of
English that/zero
variation](https://www.cambridge.org/core/journals/language-in-society/article/abs/integrating-qualitative-and-quantitative-analyses-of-stance-a-case-study-of-english-thatzero-variation/A8B82BDCBA65722367F586DBB3ED2206)
- [th-stopping in Multicultural Toronto
English](https://repository.upenn.edu/pwpl/vol26/iss2/3/)
