---
title: Output
---

This page is updated a bit less frequently than [my
CV](https://gitlab.com/gadanidis/cv/-/jobs/artifacts/master/raw/cv.pdf?job=compile_pdf),
so you may want to check there as well if you're looking for something I've
done.
If you'd like access to anything on my CV that's not yet available for download
here, please contact me at
[timothy.gadanidis@mail.utoronto.ca](mailto:timothy.gadanidis@mail.utoronto.ca)
and I'd be thrilled to share it with you.
I'm also happy to answer any questions you have about any of my research!

## Research output

### Journal articles

### Book chapters

### Presentation slides

Bigelow, Lauren, **Timothy Gadanidis**, Lisa Schlegl, Pocholo Umbal, and Derek
Denis. [[d]at’s loud, bro: A report on TH-stopping in Multicultural Toronto
English](pdf/buftor.pdf). [(pptx version with animations)](ppt/buftor.pptx) 2nd
Annual Buffalo-Toronto Workshop on Linguistic Perspectives on Variation Within
and Across Languages (Toronto). March 16, 2019.

**Gadanidis, Tim**. ['What's the *uh* for?': Pragmatic specialization of
*uh* and *um* in instant messaging](pdf/nwav47-uhum.pdf).
New Ways of Analyzing Variation (NWAV) 47.  New York University (New York City).
October 18--21, 2018.

**Gadanidis, Timothy**, Nicole Hildebrand-Edgar, Angelika Kiss, Lex Konnelly,
Katharina Pabst, Lisa Schlegl, Pocholo Umbal & Sali Tagliamonte. [Stance, style,
and semantics: Operationalizing insights from semantic-pragmatics to account for
linguistic variation](pdf/nwav47-stance.pdf). New Ways of Analyzing Variation
(NWAV) 47. New York University (New York City). October 18–21, 2018.

**Gadanidis, Tim**. ['*Um*, about that, *uh*, variable': *Uh* and *um* in teen
instant messaging](pdf/dipvac4-gadanidis.pdf).  Discourse-Pragmatic Variation &
Change (DiPVaC) 4.  University of Helsinki (Helsinki, Finland).  May 28--30, 2018.

Denis, Derek and **Tim Gadanidis**.  [Before the rise of
*um*](pdf/dipvac4-denisgadanidis.pdf). Discourse-Pragmatic Variation & Change
(DiPVaC) 4.
University of Helsinki (Helsinki, Finland).  May 28--30, 2018.

**Gadanidis, Tim**.  [I'll have to steal that... *um*, borrow it:
Investigating *uh* and *um* in the instant messages of teens and
twentysomethings](pdf/cvc10.pdf). Change & Variation in Canada (CVC) 10.
University of Manitoba and Université de Saint-Boniface (Winnipeg, Manitoba).
May 4--5, 2018.

### Manuscripts

## Software

I'm writing [a Reddit scraper](https://gitlab.com/gadanidis/reddit_scraper) in
Python to make gathering Reddit data easier for researchers interested in
qualitative discourse analysis, who might not be familiar with programming.
It glues together [PRAW](https://praw.readthedocs.io/en/stable/) and
[PSAW](https://psaw.readthedocs.io/en/latest/) to output Reddit data from a
specified time period in HTML, CSV, and/or
[pickle](https://docs.python.org/3/library/pickle.html) formats.
(Right now it might not be very useful to anyone except me.
I'm currently working on adding documentation, creating a GUI to make it more
useful for those who aren't familiar with the command line, and thinking of a
more distinctive name than "reddit\_scraper".)
