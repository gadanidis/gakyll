# gadanidis.ca
My personal website, built using [Hakyll](https://jaspervdj.be/hakyll/).

## Building

First, set up Hakyll:
1. Install [the Haskell Tool Stack](https://www.haskellstack.org): `curl -sSL
   https://get.haskellstack.org/ | sh`
2. Clone this repository: `git clone https://gitlab.com/gadanidis/gakyll`
3. Navigate to the repository, and compile: `cd gakyll && stack build`

Then, `stack exec site build` builds the website.
More info on Hakyll at [the Hakyll website](https://jaspervdj.be/hakyll/).

## Uploading

To avoid having to upload the `public_html` folder manually, I wrote a tiny
script, `sync.sh`, to do this for me.
`sync.sh` uses `lftp` to synchronize the local `public_html` folder built by
Hakyll with the remote `public_html` folder. This requires setting some
environment variables in a `config.rc` file, like in the following example:

```
FTPHOST="example.com"
FTPUSER="AzureDiamond"
FTPPASS="hunter2"
```

I use [`pass`, the standard Unix password
manager](https://www.passwordstore.org/), to avoid storing the password directly
in `config.rc`.
